using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue 
{
    public string storyName;

    [TextArea(3,10)]
    public List<string> sentences;

}
