using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryShelf : MonoBehaviour
{
    public Dialogue dialogues;

    public TextAsset textAsset;

    public List<VisualPicture> visualPictures;

    private void OnEnable()
    {
        InitializeText();
    }
    public void StartStory()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogues);
    }

    public void InitializeText()
    {
        foreach (string w in textAsset.text.Split(new char[] { '\n'}))
        {
            dialogues.sentences.Add(w);
        }
    }

 


}
[System.Serializable]
public class VisualPicture
{
    public Sprite sequencePhoto;
    public int sequenceNumber;

}
