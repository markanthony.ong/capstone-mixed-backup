    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    //Reference the Canvas
    [SerializeField] private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    public AudioSource audioSource;
    public AudioClip onClickSound;
    public AudioClip onDropSound;

    [SerializeField] private Vector3 startingPosition;


    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        startingPosition = rectTransform.anchoredPosition;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");

        audioSource.PlayOneShot(onClickSound);

        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = .6f;
    }

    public void OnDrag(PointerEventData eventData) 
    {
        Debug.Log("OnDrag");
        rectTransform.anchoredPosition += eventData.delta /canvas.scaleFactor;
 
          
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
      if (eventData.pointerEnter.tag != "AnswerSlot")
            ResetPosition();

        audioSource.PlayOneShot(onDropSound);      
      
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    public void OnDrop(PointerEventData eventData)
    { 
        
        throw new System.NotImplementedException();
    }

    public Vector3 GetStartingPosition()
    {
        return startingPosition;
    }

    public void ResetPosition()
    {
        if(rectTransform != null)
            rectTransform.anchoredPosition = startingPosition;
    }
}
