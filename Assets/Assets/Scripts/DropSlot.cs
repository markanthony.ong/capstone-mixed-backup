using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class DropSlot : MonoBehaviour, IDropHandler
{
    [SerializeField] private string answerString;
    [SerializeField] private DragDrop dragDropRef;

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log(eventData.pointerDrag.name);  
        if(eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            answerString = eventData.pointerDrag.GetComponent<Transform>().GetChild(0).GetComponent<TMP_Text>().text;
            dragDropRef = eventData.pointerDrag.GetComponent<DragDrop>();
        }
      
    }

    public string GetAnswer()
    {
        return answerString;
    }

    public DragDrop GetDragDropRef()
    {
        
        return dragDropRef;
    }

    

}
