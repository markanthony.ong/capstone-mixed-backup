using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndScreenScript : MonoBehaviour
{
    public ScoreManagerScript TheBookScore;
    public TextMeshProUGUI ScoreText;

    public GameObject continueButton;

    public AudioClip levelWinSFX;
    public AudioClip levelLoseSFX;
    public AudioSource audioSource;

    private void OnEnable()
    {
        continueButton.SetActive(GameManager.instance.GetIsTutorialOn());
        if (TheBookScore.getTotalLives() > 0)
        {
            audioSource.PlayOneShot(levelWinSFX);
        }
        else
        {
            audioSource.PlayOneShot(levelLoseSFX);
        }

    }
    public void setScoreText()
    {
        
        // This sends us info how many stars

        ScoreText.SetText((TheBookScore.getTotalLives() * 100).ToString());

        if(TheBookScore.getTotalLives() > 0 )
        {
            audioSource.PlayOneShot(levelWinSFX);
        }
        else
        {
            audioSource.PlayOneShot(levelLoseSFX);
        }
        // would set text to how many stars * 100, total of 300
    }



}
