using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{

    // The Plan
    // Click on Choice, string would send here in GameManager
    // Click on Answer, would set String to Answer
    // Would check if Answer1 and Answer2 is right, would turn red if not

    // Function to save the Choice as a string
    // Function to set Answerchecker word
    // Function to check both Answers after hitting submit

    // Mistake counter, each red or mistake ++
    // Send counter Book Clear, and shows stars and have a fail safe in the selector


    #region Singleton
    private static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if(_instance == null)
            {
                Debug.LogError("No GameManager Yet");
            }
            return _instance;

        }
    }

    private void Awake()
    {
        _instance = this;
    }
    #endregion

    [Header("Level Variables")]
    [SerializeField] List<BookManager> Shelf1;
    [SerializeField] List<BookManager> Shelf2;
    [SerializeField] List<BookManager> Shelf3;
    [SerializeField] List<BookManager> Shelf4;
    [SerializeField] private List<List<BookManager>> AllShelf = new List<List<BookManager>>();

    [SerializeField] private List<BookManager> assignedShelf = new List<BookManager>();

    public Action onFinishedLevel;

    [Header("OnBoarding Variables")]
    [SerializeField] List<BookManager> tutorialShelf1;

    [Header("On Play Variables")]

    public int LivesCounter;
    public ScoreManagerScript ScoreShelf1;
    public ScoreManagerScript ScoreEndScreen;

    public TextMeshProUGUI Answer1Text;
    public TextMeshProUGUI Answer2Text;
    public TextMeshProUGUI LivesCounterText;

    public GameObject EndScorePanel;

    [SerializeField] private BookPanelLevelTemplate bookPanelLevel;

    BookManager assignedBookData;
    [SerializeField] private AudioClip buzzerSFX;
    public AudioClip getBookSFX;
    AudioSource audioSource;


    [Header("Tutorial Variables")]
    [SerializeField] private GameObject tutorialManager;
    [SerializeField] private TutorialManager tutorialManagerScript;
    public int tutorialNumber;
    bool isTutorialOn = false;

    //shelf 2
    [SerializeField] List<Shelf2Books> shelf2Books;
    [SerializeField] Shelf2Books assignedShelf2BookData;
    [SerializeField] private Shelf2Manager shelf2LevelManager;

    //shelf 2 Tutorial
    [SerializeField] private Shelf2TutorialScript shelf2TutorialScript;
    private void Start()
    {
        // LivesCounterText.SetText(LivesCounter.ToString());
        AllShelf.Add(Shelf1);
        AllShelf.Add(Shelf2);
        AllShelf.Add(Shelf3);
        AllShelf.Add(Shelf4);
        if (this.GetComponent<AudioSource>())
            audioSource = this.GetComponent<AudioSource>();

        if (tutorialManager.GetComponent<TutorialManager>() != null)
            tutorialManagerScript = tutorialManager.GetComponent<TutorialManager>();
        tutorialNumber = 0;

    }


    public void CheckAnswers()
    {
        bool condition = bookPanelLevel.CheckAnswer();
        if (!condition)
        {
            audioSource.clip = buzzerSFX;
            audioSource.Play();
            if (bookPanelLevel.ReduceLives() <= 0 )
            {
                //ShowGameOver
                setUpEndScreen(bookPanelLevel.GetCurrentLivesLeft());
            }

        }
        if (bookPanelLevel.CheckAnswer())
        {
            setUpEndScreen(bookPanelLevel.GetCurrentLivesLeft());
        }

    }

    //shelf 2
    public void CheckShelf2Answers()
    {

        //bool shelf2Condition = shelf2LevelManager.OnAnsweringQuestions();
        
            if (shelf2LevelManager.GetCurrentShelf2LivesLeft() <=0)
            {
                Debug.Log("Wrong Answer");
                setUpEndScreen(shelf2LevelManager.GetCurrentShelf2LivesLeft());
            }
        
        if (shelf2LevelManager.GetIsComplete())
        {
            setUpEndScreen(shelf2LevelManager.GetCurrentShelf2LivesLeft());
        }

    }
    //public void CheckShelf3Answer(bool isCorrect)
    //{
    //    if(!isCorrect)
    //    {
    //        audioSource.clip = buzzerSFX;
    //        audioSource.Play();
    //        if (shelf2LevelManager.ReduceLife() <= 0)
    //        {
    //            setUpEndScreen(shelf2LevelManager.GetCurrentShelf2LivesLeft());
    //        }
    //    }
    //}

    public void setUpEndScreen(int lives)
    {
        ScoreEndScreen.setTotalLives(lives);

        // setting the score text
        EndScorePanel.GetComponent<EndScreenScript>().setScoreText();
        EndScorePanel.GetComponent<ScoreManagerScript>().setStars();


        ScoreShelf1.setStars();
        EndScorePanel.SetActive(true);
       // ResetAnswer();
    }

    public List<BookManager> OnShelfSelect(int index)
    {
        assignedShelf = AllShelf[index];
        return AllShelf[index];
    }
    // shelf 2
    public List<Shelf2Books> OnShelf2Select()
    {
        return shelf2Books;
    }
    //shelf 2
    public Shelf2Books GetCurrentShelf2BookData()
    {
        return assignedShelf2BookData;
    }

    public void AssignedBookSelected(int number)
    {
        assignedBookData = assignedShelf[number];
        isTutorialOn = false;
    }

    public void TutorialAssignedLevel()
    {
     
        assignedBookData = tutorialShelf1[tutorialNumber];
        isTutorialOn = true;
    }

    public BookManager GetCurrentBookData()
    {
        return assignedBookData;
    }

    public void TutorialSetActive(bool isActive)
    {
        tutorialManager.SetActive(isActive);

    }

    public bool GetIsTutorialOn()
    {
        return isTutorialOn;
    }

    public void SetUpTutorialShelf1()
    {
        tutorialNumber++;
        EndScorePanel.SetActive(false);
        if (tutorialNumber >= 0 && tutorialNumber < tutorialShelf1.Count)
        {
            TutorialAssignedLevel();
            bookPanelLevel.SetupTemplate(assignedBookData);
       
        }
        else
        {
            tutorialNumber = 0;
            TutorialAssignedLevel();
            bookPanelLevel.SetupTemplate(assignedBookData);
            //bookPanelLevel.gameObject.SetActive(false);
        }
    }

    public void PlayBookSFX()
    {
        audioSource.PlayOneShot(getBookSFX);
    }

}
