using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    [Header("UI Panels")]
    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject shelfMenuPanel;
    [SerializeField] private GameObject bookSelectionPanel;
    [SerializeField] private GameObject bookLevelTemplatePanel;
    [SerializeField] private GameObject bookLevelTutorialPanel;
    [SerializeField] private GameObject endScreenPanel;
    [SerializeField] private GameObject optionsPanel;
    public GameObject Level3TemplatePanel;


    [Header("Scripts")]
    [SerializeField] private BookSelectionPanel bookSelectionPanelScript;
    [SerializeField] private BookPanelLevelTemplate bookPanelLevelTemplateScript;
    public TutorialManager tutorialManager;
    //shelf 2
    [SerializeField] private List<Shelf2Books> shelf2Book;
    [SerializeField] private List<GameObject> books;
    [SerializeField] private GameObject shelf2TemplatePanel;

    [Header("Audios")]
    public AudioSource audioSource;
    public AudioClip mainMenuSFX;
    public AudioClip shelfMenuSFX;
    public AudioClip shelf1SFX;
    public AudioClip shelf2SFX;
    public AudioClip shelf3SFX;


    private void Start()
    {
        if (bookSelectionPanel != null)
            bookSelectionPanelScript = bookSelectionPanel.GetComponent<BookSelectionPanel>();
        if (bookLevelTemplatePanel != null)
            bookPanelLevelTemplateScript = bookLevelTemplatePanel.GetComponent<BookPanelLevelTemplate>();
    }
    public void OnPlayButtonClicked()
    {
        mainMenuPanel.SetActive(false);
        shelfMenuPanel.SetActive(true);
    }


    public void OnShelfButtonClicked(int number)
    {
        List<BookManager> assignedShelf = GameManager.instance.OnShelfSelect(number);
        bookSelectionPanelScript.SetUpSelectionPanel(assignedShelf);
        bookSelectionPanel.SetActive(true);
        AddButtonEventShelf1();
    }

    //Shelf 2
    public void OnShelf2ButtonClicked()
    {
        List<Shelf2Books> assignedShelf = GameManager.instance.OnShelf2Select();
        bookSelectionPanelScript.SetUpSelectionPanel2(assignedShelf);
        bookSelectionPanel.SetActive(true);
        buttonEvent();

    }

    public void OnShelf3ButtonClicked()
    {
        for (int i = 0; i < books.Count - 1; i++)
        {

            books[i].transform.GetChild(0).GetComponent<TMP_Text>().text = "Story " + (i + 1);
        }
        bookSelectionPanel.SetActive(true);
        bookSelectionPanelScript.SetUpSelectionPanel3();
        AddButtonEventShelf3();
    }
    public void OnBookClicked(int number)
    {
        if (number == 4) //Tutorial
        {
            GameManager.instance.TutorialAssignedLevel();
            GameManager.instance.TutorialSetActive(true);
            tutorialManager.enabled = true;
            bookLevelTemplatePanel.SetActive(true);
            bookPanelLevelTemplateScript = bookLevelTemplatePanel.GetComponent<BookPanelLevelTemplate>();
        }
        else
        {
            GameManager.instance.AssignedBookSelected(number);
            bookLevelTemplatePanel.SetActive(true);
            bookPanelLevelTemplateScript = bookLevelTemplatePanel.GetComponent<BookPanelLevelTemplate>();
        }

        SetUpPlaySFX(shelf1SFX);
        //bookPanelLevelTemplateScript.SetupTemplate(GameManager.instance.AssignedBookSelected(number));


    }
    //shelf 2
    public void OnShelf2BookClicked(int index)
    {

        shelf2Book[index].GetQuestionPrefab();
        shelf2Book[index].GetAnswers();
        shelf2Book[index].GetBookImage();
        shelf2TemplatePanel.SetActive(true);
        shelf2TemplatePanel.GetComponent<Shelf2Manager>().SetupShelf2Book(shelf2Book[index]);
        SetUpPlaySFX(shelf2SFX);
        if (index == 4)
        {
            shelf2TemplatePanel.GetComponent<Shelf2Manager>().InitializeTutorial(true);
        }
        else
        {
            shelf2TemplatePanel.GetComponent<Shelf2Manager>().InitializeTutorial(false);
        }
    }

    //Shelf 2 Event Add
    public void buttonEvent()
    {
        foreach (GameObject button in books)
        {
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        books[0].GetComponent<Button>().onClick.AddListener(delegate { OnShelf2BookClicked(0); });
        books[1].GetComponent<Button>().onClick.AddListener(delegate { OnShelf2BookClicked(1); });
        books[2].GetComponent<Button>().onClick.AddListener(delegate { OnShelf2BookClicked(2); });
        books[3].GetComponent<Button>().onClick.AddListener(delegate { OnShelf2BookClicked(3); });
        books[4].GetComponent<Button>().onClick.AddListener(delegate { OnShelf2BookClicked(4); });
    }
    //Shelf 1 Event Add
    public void AddButtonEventShelf1()
    {

        foreach (GameObject button in books)
        {
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        books[0].GetComponent<Button>().onClick.AddListener(delegate { OnBookClicked(0); });
        books[1].GetComponent<Button>().onClick.AddListener(delegate { OnBookClicked(1); });
        books[2].GetComponent<Button>().onClick.AddListener(delegate { OnBookClicked(2); });
        books[3].GetComponent<Button>().onClick.AddListener(delegate { OnBookClicked(3); });
        //Tutorial
        books[4].GetComponent<Button>().onClick.AddListener(delegate { OnBookClicked(4); });
    }

    //Shelf 3 Event Add
    public void AddButtonEventShelf3()
    {
        foreach (GameObject button in books)
        {
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        books[0].GetComponent<Button>().onClick.AddListener(delegate { OnShelf3BookClicked(0); });
        books[1].GetComponent<Button>().onClick.AddListener(delegate { OnShelf3BookClicked(1); });
        books[2].GetComponent<Button>().onClick.AddListener(delegate { OnShelf3BookClicked(2); });
        books[3].GetComponent<Button>().onClick.AddListener(delegate { OnShelf3BookClicked(3); });
        //Tutorial
        books[4].GetComponent<Button>().onClick.AddListener(delegate { OnShelf3BookClicked(4); });
    }
    public void OnQuitButtonClick()
    {
        Application.Quit();
    }
    public void ReturnMainMenu()
    {
        mainMenuPanel.SetActive(true);
        SetUpPlaySFX(mainMenuSFX);
    }

    public void OnShelf3BookClicked(int index)
    {
        if (index == 4)
        {
            DialogueManager._instance.QuestionTemplate.SetActive(true);
            DialogueManager._instance.questionTemplateScript.isTutorialDone = false;
            DialogueManager._instance.questionTemplateScript.enabled = true;
            DialogueManager._instance.AssignedQuestionList(4);


        }
        else
        {
            DialogueManager._instance.SetAssignedStoryShelf(index);
            Level3TemplatePanel.SetActive(true);

        }
        SetUpPlaySFX(shelf3SFX);
    }

    public void SetUpPlaySFX(AudioClip audioClip)
    {
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.Play();
    }

}
