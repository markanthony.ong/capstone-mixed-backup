using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaneManagerScript : MonoBehaviour
{
    public GameObject ShelfPanel;
    public GameObject BookPanelTemplate;

    int WhichShelf;
    int WhichBookPanel;
    
    // preferred Idea is to set which is the active Shelf
    // same for the Book Selection, set which shelf
    // no functions yet

    public int setWhichShelf(int ShelfSetAs)
    {
        return WhichShelf = ShelfSetAs;
        // this would send which Shelf number we are at
    }

    public int setWhichBookPanel(int BookPanelSetAs)
    {
        return WhichBookPanel = BookPanelSetAs;
        // Sets which Book to open, we are going to keep them by number na here 1-16
    }

}
