using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelActivator : MonoBehaviour
{
    public int SendInfo;
    public bool IsShelf = true;

    // Type which to send
    // Examples: Shelf 1, BookPanel 15

    // Sorry if the name is wrong, Send Shelf Info

    public void SendInfoPanelManager()
    {
        if (IsShelf)
        {
            FindObjectOfType<PaneManagerScript>().setWhichShelf(SendInfo);
        }

        else if (!IsShelf)
        {
            FindObjectOfType<PaneManagerScript>().setWhichBookPanel(SendInfo);
        }
    }
    // This would find the PaneManagerScript-> set which Shelf, to have a book mark

}
