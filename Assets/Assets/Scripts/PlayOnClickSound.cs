using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnClickSound : MonoBehaviour
{
    public AudioSource OnClickSound;

    public void PlaySound()
    {
        if(OnClickSound != null)
            OnClickSound.Play();
    }
}
