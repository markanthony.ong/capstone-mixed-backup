using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Scriptable Object", menuName = "Scriptable Objects/New Scriptable Object")]
public class SO_Sample : ScriptableObject
{
    public string scriptableObjectName;
    public AudioClip audioClip;
    [TextArea]
    public string text;
}
