using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ScoreManagerScript : MonoBehaviour
{
    // The Plan here
    // Keeps the score for the books
    // each one has 3 stars max
    // would would deduct each star mistakes

    public Image[] Stars;
    public Sprite EmptyStar;
    public Sprite FullStar;
    public TextMeshProUGUI ScoreText;

    int totalLives = 3;
    int totalLivesBefore = 0;

    int starsBook1 = 0; // gonna make only one for testing and demo

    public void setTotalLives(int settingLives)
    {
        totalLives = settingLives;
    }

    public void setStars()
    {
        // resetting the star
        resetStars();

        if (totalLives == 0)
        {
            // 0 stars
            starsBook1 = totalLives;
            Debug.Log("0 Stars");
          
        }

        else if (totalLives == 1)
        {
            // 1 stars
            starsBook1 = totalLives;
            Debug.Log("1 Stars");
        }

        else if (totalLives == 2)
        {
            // 2 stars
            starsBook1 = totalLives;
            Debug.Log("2 Stars");
        }

        else if (totalLives >= 3)
        {
            // 3 stars
            starsBook1 = totalLives;
            Debug.Log("3 Stars");
        }

        for (int i = 0; i < starsBook1; i++)
        { 
            Stars[i].sprite = FullStar;
        }

    }

    public void resetStars()
    {
       for (int i = 0; i < Stars.Length; i++)
       {
            Stars[i].sprite = EmptyStar;
       }
    }

    // use lives since it updates instantly on the game manager
    public int getTotalLives()
    {
        return totalLives;
    }
}
