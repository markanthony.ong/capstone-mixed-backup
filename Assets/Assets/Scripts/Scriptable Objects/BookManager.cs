using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "BookData", menuName = "BookData")]
public class BookManager : ScriptableObject
{
    [SerializeField] private string shelfTitle;
    [SerializeField] private string lessonsName;
    [SerializeField] private List<string> nounChoices, verbChoices, adjectChoices, adverbChoices, prepositionChoices = new List<string>();


    //   private List<List<string>> choices = new List<List<string>>();
    //[SerializeField] private Dictionary<string, string> answer = new Dictionary<string, string>();

    [SerializeField] private List<string> answers;
    [SerializeField] private Sprite imageArt;
    [SerializeField] private bool isCompleted = false;
    [SerializeField] private int starCount = 0;
    [SerializeField] private GameObject sentencePrefab;
    [SerializeField] private GameObject correctSentencePrefab;



    public string GetLevelName()
    {
        return lessonsName;
    }

    public string GetShelfTopicName()
    {
        return shelfTitle;
    }
    public List<string> GetAdverbChoices()
    {
        return adverbChoices;
    }
    public List<string> GetNounChoices()
    {
        return nounChoices;
    }

    public List<string> GetVerbChoices()
    {
        return verbChoices;
    }

    public List<string> GetAdjectChoices()
    {
        return adjectChoices;
    }

    public List<string> GetPrepositionChoices()
    {
        return prepositionChoices;
    }

    public void GetAnswers(List<string> listString)
    {
        if (listString.Count >= 1)
            listString.Clear();
       foreach(string a in answers)
       {
            listString.Add(a);
       }
    }

    public bool GetIsCompleted()
    {
        return isCompleted;
    }

    private void SetIsCompleted(bool condition)
    {
        isCompleted = condition;
    }

    public Sprite GetImageArt()
    {
        return imageArt;
    }

    public GameObject GetSentencePrefab()
    {
        return sentencePrefab;
    }

    public GameObject GetCorrectSentencePrefab()
    {
        return correctSentencePrefab;
    }
}
