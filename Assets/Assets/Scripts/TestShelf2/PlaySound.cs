using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlaySound : MonoBehaviour
{
    [SerializeField] private AudioSource soundPlayer;
    [SerializeField] private List<AudioClip> audClips;
    [SerializeField] private List<Button> buttons;

    // Start is called before the first frame update


    public void playSoundClip1()
    {
          soundPlayer.clip = audClips[0];
          soundPlayer.Play();
    }
    public void playSoundClip2()
    {
        soundPlayer.clip = audClips[1];
        soundPlayer.Play();
    }
    public void playSoundClip3()
    {
        soundPlayer.clip = audClips[2];
        soundPlayer.Play();
    }
    public void playSoundClip4()
    {
        soundPlayer.clip = audClips[3];
        soundPlayer.Play();
    }
    public void playSoundClip5()
    {
        soundPlayer.clip = audClips[4];
        soundPlayer.Play();
    }
    public void playSoundClip6()
    {
        soundPlayer.clip = audClips[5];
        soundPlayer.Play();
    }
}
