using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName = "New Book", menuName = "Shelf 2 Book")]
public class Shelf2Books : ScriptableObject
{

    [SerializeField] private int bookNumber;
    [SerializeField] private string difficulty;
    [SerializeField] private string shelfTitle;
    

    //[SerializeField] private List<string> answers;
    [SerializeField] private List<string> answers;
    [SerializeField] private List<string> tutorialWords;
    [SerializeField] private List<AudioClip> audioFiles;
    [SerializeField] private GameObject Shelf2QuestionsPrefab;
    [SerializeField] private bool isComplete = false;
    [SerializeField] private Sprite bookImage;

    [SerializeField] private GameObject tutorialPrefabs;

    public string GetShelfTitle()
    {
        return shelfTitle;
    }
    public string GetDifficulty()
    {
        return difficulty;
    }
    public GameObject GetQuestionPrefab()
    {
        return Shelf2QuestionsPrefab;
    }
    public int GetbookNumber()
    {
        return bookNumber;
    }
    public List<string> GetAnswers()
    {
        return answers;
    }
    public List<string> GetTutorialWords()
    {
        return tutorialWords;
    }
    /*public List<string> GetListOfAnswers()
    {
        return listOfAnswers;
    }*/
    public List<AudioClip> GetAudio()
    {
        return audioFiles;
    }

    public bool GetIsComplete()
    {
        return isComplete;
    }
    
    private void SetIsComplete(bool condition)
    {
        isComplete = condition;
    }
    public Sprite GetBookImage()
    {
        return bookImage;
    }

    public GameObject GetTutorialPrefab()
    {
        return tutorialPrefabs;
    }
}
