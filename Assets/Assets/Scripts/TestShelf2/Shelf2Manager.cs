using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Shelf2Manager : MonoBehaviour
{
    public Shelf2Books shelf2Data;
    [SerializeField] private Shelf2QuestionAnswer shelf2QA;
    public Shelf2TutorialScript shelf2TutorialData;

    /*[SerializeField] private TMP_Text sentences;
    [SerializeField] private TMP_Text bookNum;*/
    [SerializeField] private List<string> listOfAnswer = new List<string>();
    [SerializeField] private List<string> listOfGuideWords = new List<string>();
 
    [SerializeField] private int itemCount = 0;
    [SerializeField] private int levelNum;
    [SerializeField] private Button submitButton;
    [SerializeField] private Button backButton;
    [SerializeField] private GameObject questionPrefab;

    [SerializeField]private List<TMP_Text> answerDis;
    [SerializeField] private TMP_InputField answerArea;
    [SerializeField] private List<TMP_Text> hintText;

    [SerializeField] private int lifeCount;
    [SerializeField] private List<Image> lifeIcons;

    [SerializeField] private AudioClip wrongAnswer;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private Image imageArt;
    [SerializeField] private bool IsComplete;

    [SerializeField] private ShakeCamera shakeCamera;
    [SerializeField] private GameObject shelf2TutorialSentences;
    [SerializeField] private bool IsTutorial;
    [SerializeField] private List<Image> superScripts;
    

    void Start()
    {
        shakeCamera = this.GetComponent<ShakeCamera>();
    }


    void Update()
    {
        
    }

    public void SetupShelf2Book(Shelf2Books shelf2BookData)
    {
        shelf2Data = shelf2BookData;
        if (questionPrefab != null)
        {
            GameObject.Destroy(questionPrefab);
        }

       if(shelf2BookData.GetQuestionPrefab() != null)
        {
            questionPrefab = Instantiate(shelf2BookData.GetQuestionPrefab(), this.gameObject.transform);
            questionPrefab.transform.SetSiblingIndex(1);
        }
        hintText = questionPrefab.GetComponent<Shelf2QuestionAnswer>().GetGuideLetters();
        answerDis = questionPrefab.GetComponent<Shelf2QuestionAnswer>().GetAnswerAreas();
        superScripts = questionPrefab.GetComponent<Shelf2QuestionAnswer>().GetSuperScriptsImages();
        
        

        listOfAnswer = shelf2Data.GetAnswers();
        lifeCount = 3;
        resetLives();
        resetItemCount();
        IsComplete = false;

       if(shelf2BookData.GetBookImage() != null)
        {
              imageArt.sprite = shelf2BookData.GetBookImage();
        }
  
    }

    public void OnAnsweringQuestions()
    {

        int itemCount2 = itemCount;
  
        if (answerArea.text == listOfAnswer[itemCount])
        {
            Debug.Log("right");
            
            answerDis[itemCount].text = listOfAnswer[itemCount];

            superScripts[itemCount].GetComponentInChildren<TMP_Text>().text = ""; // makes text blank
            Destroy(superScripts[itemCount]);// makes superscript disappear
            answerArea.text = ""; // reset input area when getting correct answers  
            itemCount++;

            if(itemCount >= listOfAnswer.Count)
            {                
                IsComplete = true;
                GameManager.instance.CheckShelf2Answers(); // shows the endscreen if game is finished
                ResetListOfAnswers();
            }
        }
        else if (answerArea.text != listOfAnswer[itemCount])
        {          
            ReduceLife();
            audioSource.PlayOneShot(wrongAnswer); // plays the audio
            GameManager.instance.CheckShelf2Answers();
            answerArea.text = "";

            shakeCamera.StartShake();

            if(hintText.Count != 0)
            {
                hintText[itemCount2].text = listOfAnswer[itemCount2];
            }
        }
    }

    public void OnClickBackButton()
    {
        GameObject.Destroy(questionPrefab);
        InitializeTutorial(false);
    }
    public void resetLives()
    {
        foreach(Image i in lifeIcons)
        {
            i.color = Color.white;
        }
        
    }
   public void resetItemCount()
    {
        if(itemCount > 0)
        itemCount = 0;
       
    }
   public bool GetIsComplete()
    {
       
        return IsComplete;
    }
    public int ReduceLife()
    {
        lifeIcons[lifeCount - 1].color = Color.black;
        lifeCount--;
        return lifeCount;
    }

    public int GetCurrentShelf2LivesLeft()
    {
        return lifeCount;
    }
    public void ResetListOfAnswers()
    {
        listOfAnswer = null;
    }

    public void InitializeTutorial(bool isTutorial) // starts up tutorial 
    {      
        shelf2TutorialSentences.SetActive(isTutorial);
        if (isTutorial == false)
        {
            shelf2TutorialData.resetTutorial();
        }

    }
    public void ReinitializeTutorial() // use for submit button
    {
        shelf2TutorialSentences.SetActive(false);
        shelf2TutorialData.resetTutorial();
    }
    
}
