using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Shelf2QuestionAnswer : MonoBehaviour
{
    [SerializeField] private List<TMP_Text> answerDisplays;
    [SerializeField] private List<Button> audioButtons;
    [SerializeField] private List<TMP_Text> guideLetters;
    [SerializeField] private List<Image> superScriptsImages;

    public List<TMP_Text> GetAnswerAreas()
    {
        return answerDisplays;
    }

    public List<Button> GetButtons()
    {
        return audioButtons;
    }

    public List<TMP_Text> GetGuideLetters()
    {
        return guideLetters;
    }

    public List<Image> GetSuperScriptsImages()
    {
        return superScriptsImages;
    }

}
