using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Shelf2TutorialScript : MonoBehaviour
{
    [SerializeField] private GameObject Panel;
    public List<TMP_Text> tutorialPopUps;
    private Queue<TMP_Text> tutorialTextQueue;
    private int popUpIndex;
    public bool isComplete = false;
    // Start is called before the first frame update

    private void OnEnable()
    {
        tutorialTextQueue = new Queue<TMP_Text>();
        Panel.SetActive(true);
        isComplete = false;
        InitializeSentences();
        //resetTutorial();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            DisplayTutorialPopUps();
        }

        //resetTutorial();
    }

   void DisplayTutorialPopUps()
    {
        if(tutorialTextQueue.Count == 0)
        {
            isComplete = true;
            Panel.SetActive(false);
            this.enabled = false;
            return;
        }
        TMP_Text currentPopUp = tutorialTextQueue.Dequeue();

        Panel.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = currentPopUp.text;
    }

    void InitializeSentences()
    {
        
        foreach (TMP_Text tutorialLines in tutorialPopUps)
        {
            tutorialTextQueue.Enqueue(tutorialLines);
        }
        DisplayTutorialPopUps();
    }

   public void resetTutorial() // reinitializes the tutorial dialogue
    {
        tutorialTextQueue = new Queue<TMP_Text>();
        if (tutorialTextQueue.Count == 0)
        {
            //tutorialTextQueue = new Queue<TMP_Text>();
            Panel.SetActive(true);
            isComplete = false;
            this.enabled = true;
            return;
        }
        InitializeSentences();

    }
   
}
