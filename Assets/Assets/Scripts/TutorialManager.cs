using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TutorialManager : MonoBehaviour
{
    [SerializeField]

    public TextAsset tutorialTextAsset;
    private Queue<string> sentenceQueue;
    public List<string> sentences;

    public GameObject panel;
    public int tutorialCount;
    public bool isTutorialDone = false;

    [SerializeField] private TMP_Text tutorialTextUI;


    private void OnEnable()
    {
        sentenceQueue = new Queue<string>();
        panel.SetActive(true);
        isTutorialDone = false;
        InitializeText();
    }

    private void InitializeText()
    {
        sentences.Clear();
        foreach (string w in tutorialTextAsset.text.Split(new char[] { '\n' }))
        {
            sentences.Add(w);
        }

        foreach (string lines in sentences)
        {
            sentenceQueue.Enqueue(lines);
        }
        DisplayNextTutorialLine();
    }

    private void DisplayNextTutorialLine()
    {
        if (sentenceQueue.Count == 0)
        {
            isTutorialDone = true;
            panel.SetActive(false);
            this.enabled = false;
            return;

        }
        string currentLine = sentenceQueue.Dequeue();

        panel.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = currentLine;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DisplayNextTutorialLine();
        }
    }
    //void ShowTutorialLines(int number)
    //{
    //    if (isTutorialDone)
    //    {
    //        panel.SetActive(false);
    //        return;

    //    }
    //    Debug.Log(number);

    //    tutorialTextBoxes[0].SetActive(true);
    //    tutorialTextUI = tutorialTextBoxes[0].GetComponentInChildren<TMP_Text>();

    //    tutorialTextUI.text = "";


    //    tutorialTextUI.text = tutorialLines[number];

    //    count++;

    //    if (count >= tutorialLines.Count)
    //    {
    //        ResetTutorial();
    //    }

    //}

    //void InitializeWords()
    //{
    //    int wordsIndex = 0;
    //    foreach (string w in tutorialText.text.Split(new char[] { '\n' }))
    //    {
    //        string getTutorialText = w.Trim();
    //        if (getTutorialText == "=")
    //        {
    //            wordsIndex++;
    //            continue;
    //        }

    //        switch (wordsIndex)
    //        {
    //            case 0:
    //                tutorialLines.Add(w);
    //                break;
    //            case 1:
    //                tutorialLines2.Add(w);
    //                break;
    //            case 2:
    //                tutorialLines3.Add(w);
    //                break;
    //            case 3:
    //                tutorialAnswers.Add(w);
    //                break;
    //        }
    //    }
    //}
   
}
