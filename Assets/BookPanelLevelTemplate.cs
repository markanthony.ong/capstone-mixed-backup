using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BookPanelLevelTemplate : MonoBehaviour
{
    [Header("Basic Level Setups")]
    [SerializeField] private GameObject choicePrefab;
    [SerializeField] private Image imageArt;
    
    [Header("Choices Prefabs")]
    [SerializeField] private List<GameObject> NounChoices;
    [SerializeField] private List<GameObject> VerbChoices;
    [SerializeField] private List<GameObject> AdjectChoices;
    [SerializeField] private List<GameObject> AdverbChoices;
    [SerializeField] private List<GameObject> PrepositionChoices;
    [SerializeField] private List<GameObject> answerSlots;

    [Header("Text Prefabs")]
    [SerializeField] private GameObject NounText;
    [SerializeField] private GameObject VerbText;
    [SerializeField] private GameObject AdjectText;
    [SerializeField] private GameObject AdverbText;
    [SerializeField] private GameObject PrepositionText;

    [Header("Live Counts & Heart Image")]
    [SerializeField] private List<Image> heartLives;
    [SerializeField] private int liveCount = 3;
        
    [SerializeField] private BookManager assignedBookLevel;

    [SerializeField] private List<string> listofAnswers = new List<string>();

    [SerializeField] private GameObject sentence;
    [SerializeField] private GameObject tutorialCorrectSentence;
    [SerializeField] private ShakeCamera shakeCamera;

    private void OnEnable()
    {
  
        SetupTemplate(GameManager.instance.GetCurrentBookData());
        shakeCamera = this.GetComponent<ShakeCamera>();
   
    }
    public void SetupTemplate(BookManager bookLevelData)
    {
        //To position as a first child 
        if(sentence != null)
        {
            GameObject.Destroy(sentence);
        }
        if(tutorialCorrectSentence != null)
        {
            GameObject.Destroy(tutorialCorrectSentence);
        }
        if(bookLevelData.GetSentencePrefab() != null)
        {
            sentence = Instantiate(bookLevelData.GetSentencePrefab(), this.gameObject.transform);
            
            //Fixing UI Issues 
            sentence.GetComponent<RectTransform>().SetAsFirstSibling();
        }

        if(bookLevelData.GetCorrectSentencePrefab() != null)
        {
            
           tutorialCorrectSentence = Instantiate(bookLevelData.GetCorrectSentencePrefab(), this.gameObject.transform);
        }

        if(bookLevelData.GetImageArt() != null)
        {
            imageArt.gameObject.SetActive(true);
            imageArt.sprite = bookLevelData.GetImageArt();
        }
        else
        {
            imageArt.gameObject.SetActive(false);
        }
      
    
        answerSlots = sentence.GetComponent<BookQuestionScript>().GetDropSlots();


        liveCount = 3;

        ResetLives();

        assignedBookLevel = bookLevelData;
        assignedBookLevel.GetAnswers(listofAnswers);

        //Choices Fill up. 
        FillUpChoices(bookLevelData.GetNounChoices(), NounChoices, NounText);     
        FillUpChoices(bookLevelData.GetVerbChoices(), VerbChoices, VerbText);
        if(bookLevelData.GetAdjectChoices() != null)
            FillUpChoices(bookLevelData.GetAdjectChoices(), AdjectChoices, AdjectText);
        if(bookLevelData.GetAdverbChoices() != null)
            FillUpChoices(bookLevelData.GetAdverbChoices(), AdverbChoices, AdverbText);
        if (bookLevelData.GetPrepositionChoices() != null)
            FillUpChoices(bookLevelData.GetPrepositionChoices(), PrepositionChoices, PrepositionText);

    }

    //This fill up Choices Automatically based on book level data choices
    void FillUpChoices(List<string> choices, List<GameObject> gameObjects, GameObject textPrefab)
    {
        foreach (GameObject gO in gameObjects)
        {
            gO.SetActive(false);
        }
        RestartChoicesPos(gameObjects);
        //If No choices available, the Choices GameObject will not be shown
        if (choices.Count <= 0)
        {
            foreach(GameObject gO in gameObjects)
            {
                gO.SetActive(false);
            }
            textPrefab.SetActive(false);
            return;
        }

        //If there are any choices, Fill Up;
        for (int i = 0; i < choices.Count; i++)
        {
            textPrefab.SetActive(true);
            gameObjects[i].SetActive(true);
            gameObjects[i].GetComponentInChildren<TMP_Text>().text = choices[i];
        }
       
    }

    public int ReduceLives()
    {
        //make Heart look disappear or  removed by changing color
       // answerSlots

        heartLives[liveCount - 1].color = Color.black;
        liveCount--;

        return liveCount;
    }

    private void ResetLives()
    {
        foreach(Image i in heartLives)
        {
            i.color = Color.white;
        }
    }

    public bool CheckAnswer()
    {
        bool isCorrect = false;
         
        for (int i = 0; i < answerSlots.Count; i++)
        {
            
            if(listofAnswers[i] == answerSlots[i].GetComponent<DropSlot>().GetAnswer())
            {
                isCorrect = true;
            }
            else
            {

                shakeCamera.StartShake();
                RestartChoicesPos(answerSlots);
                return false;
            }
        }

        return isCorrect;
    }

    public int GetCurrentLivesLeft()
    {
        return liveCount;
    }

    public void RestartChoicesPos(List<GameObject> gameObjects)
    {
        foreach (GameObject go in gameObjects)
        {
            if (go.GetComponent<DragDrop>())
                go.GetComponent<DragDrop>().ResetPosition();

            else if (go.GetComponent<DropSlot>() && go.GetComponent<DropSlot>().GetDragDropRef())
                go.GetComponent<DropSlot>().GetDragDropRef().ResetPosition();
            
        }
    }

}
