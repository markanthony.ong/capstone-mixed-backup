using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookQuestionScript : MonoBehaviour
{
    [SerializeField] private List<GameObject> dropSlots;
    // Start is called before the first frame update
    
    public List<GameObject> GetDropSlots()
    {
        return dropSlots;
    }
}
