using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BookSelectionPanel : MonoBehaviour
{

    [SerializeField] private List<TMP_Text> bookTexts = new List<TMP_Text>();
    [SerializeField] private TMP_Text titleText;
    public void SetUpSelectionPanel(List<BookManager> shelf)
    {
        titleText.text = shelf[0].GetShelfTopicName();

        for (int i = 0; i < shelf.Count; i++)
        {
            bookTexts[i].text = shelf[i].GetLevelName();
        }
    }

    //shelf 2
    public void SetUpSelectionPanel2(List<Shelf2Books> shelf)
    {
        titleText.text = shelf[0].GetShelfTitle();

        for (int i = 0; i < shelf.Count; i++)
        {
            bookTexts[i].text = shelf[i].GetDifficulty();
        }
    }

    public void SetUpSelectionPanel3()
    {
        titleText.text = "Shelf 3 Reading Comprehension";
    }
}
