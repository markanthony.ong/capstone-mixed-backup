using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    #region Singleton
    public static DialogueManager _instance { get; private set; }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    private Queue<string> sentences;

    public List<GameObject> storyShelves;

    public StoryShelf assignedStoryShelf;
    public List<GameObject> assignedQuestionList;

    [Header("Shelf 3 Template")]
    public GameObject shelf3Template;
    public ShakeCamera shakeCamera;

    public Image image;
    public TMP_Text textPanel;
    public TMP_Text textTitle;

    public int sentenceCount;
    public int imageCount;
    public int displayCount;
    public int questionCount;
    public float delay;
    [Header("Live Counts & Heart Image")]
    [SerializeField] private List<Image> heartLives;
    [SerializeField] private int liveCount = 3;


    [Header("Question Variables")]
    public GameObject QuestionTemplate;
    public TMP_Text questionTextBox;

    List<List<GameObject>> questionLists = new List<List<GameObject>>();
    public List<GameObject> story1Questions;
    public List<GameObject> story2Questions;
    public List<GameObject> story3Questions;
    public List<GameObject> story4Questions;
    public List<GameObject> onboardingQuestions;
    GameObject questionRef;

    [Header("Tutorial Related Variables")]
    public GameObject panel;
    public QuestionTemplateScript questionTemplateScript;

    [Header("Prompts UI")]
    public GameObject correctImage;
    public GameObject wrongImage;
    public GameObject endOfStoryPrompt;

    [Header("Sound Effects")]
    public AudioClip correctSFX;
    public AudioClip wrongSFX;
    public AudioClip finishedSFX;


    GameObject ob;
    bool isInStory;
    private void Start()
    {
        sentences = new Queue<string>();
        shakeCamera = QuestionTemplate.GetComponent<ShakeCamera>();
        questionTemplateScript = QuestionTemplate.GetComponent<QuestionTemplateScript>();
        imageCount = 0;
        sentenceCount = 0;
        questionLists.Add(story1Questions);
        questionLists.Add(story2Questions);
        questionLists.Add(story3Questions);
        questionLists.Add(story4Questions);
        questionLists.Add(onboardingQuestions);
        ResetLives();
       // SetAssignedStoryShelf(1);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isInStory)
        {
            DisplayNextSentence();
        }
    }
    public void StartDialogue(Dialogue dialogue)
    {
        //Debug.Log("Start Story" + dialogue.name);

        sentences.Clear();
        imageCount = 0;

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        string initialSentence = sentences.Dequeue();

        StopAllCoroutines();
        StartCoroutine(TypeSentence(initialSentence));
        DisplayNextSentence();

    }
    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();

            return;
        }

        //image.sprite = assignedStoryShelf.visualPictures[imageCount].sequencePhoto;

        //if (assignedStoryShelf.visualPictures[imageCount].sequenceNumber <= 0)
        //{
        //    imageCount++;
        //    image.sprite = assignedStoryShelf.visualPictures[imageCount].sequencePhoto;
        //    return;
        //}
        //else if (assignedStoryShelf.visualPictures[imageCount].sequenceNumber > 0)
        //{
        //    if (sentenceCount == assignedStoryShelf.visualPictures[imageCount].sequenceNumber)
        //    {
        //        sentenceCount = 0;
        //        imageCount++;
        //        return;
        //    }
        //    else if (sentenceCount < assignedStoryShelf.visualPictures[imageCount].sequenceNumber)
        //    {

        //        sentenceCount++;
        //    }

        //}

        //show Image
        image.sprite = assignedStoryShelf.visualPictures[imageCount].sequencePhoto;


        if (assignedStoryShelf.visualPictures[imageCount].sequenceNumber <= 0)
        {
            imageCount++;

        }
        else
        {
            if (sentenceCount < assignedStoryShelf.visualPictures[imageCount].sequenceNumber && assignedStoryShelf.visualPictures[imageCount].sequenceNumber > 0)
            {
                string sentence = sentences.Dequeue();

                StopAllCoroutines();
                StartCoroutine(TypeSentence(sentence));
                sentenceCount++;
            }
            else if (sentenceCount >= assignedStoryShelf.visualPictures[imageCount].sequenceNumber && assignedStoryShelf.visualPictures[imageCount].sequenceNumber > 0)
            {
                sentenceCount = 0;
                imageCount++;
            }
        }




    }

    IEnumerator TypeSentence(string sentence)
    {
        textPanel.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            textPanel.text += letter;
            yield return new WaitForSeconds(delay);
        }
    }
    public void SetAssignedStoryShelf(int index)
    {
        Debug.Log("Set Assigned Story Shelf");
        isInStory = true;
        endOfStoryPrompt.SetActive(false);
        ob = Instantiate(storyShelves[index]);
        assignedStoryShelf = ob.GetComponent<StoryShelf>();
        assignedQuestionList = questionLists[index];
        
        imageCount = 0;
        sentenceCount = 0;
        questionCount = 0;

        ResetLives();
        StartDialogue(assignedStoryShelf.dialogues);

        if (shelf3Template != null)
        {
            shelf3Template.SetActive(true);
        }
    }

    public void AssignedQuestionList(int index)
    {
        Debug.Log("Assigned Question List Called");
        assignedQuestionList = questionLists[index];
        ResetLives();
        ShowNextQuestion();
    }

    public void EndDialogue()
    {
        Debug.Log("End Story");
        isInStory = false;
        endOfStoryPrompt.SetActive(true);
        StartCoroutine(WaitForFinish());
        GameObject.Destroy(ob);
    }

    public void ShowNextQuestion()
    {
        if (questionCount >= assignedQuestionList.Count)
        {
            return;
        }

        Destroy(questionRef);

        questionRef = Instantiate(assignedQuestionList[questionCount], QuestionTemplate.transform);
        questionRef.GetComponent<RectTransform>().SetAsFirstSibling();
        QuestionTemplate.SetActive(true);
        wrongImage.SetActive(false);
        correctImage.SetActive(false);
        questionTextBox.text = questionRef.GetComponent<VisualNovelQuestion>().questionString;
        textTitle.text = "Question " + (questionCount + 1) + "/" +  assignedQuestionList.Count;
       


    }

    public void CheckAnswer(bool isCorrect)
    {
        if (isCorrect)
        {
            questionCount++;
            if (questionCount >= assignedQuestionList.Count)
            {
                //Show Stars Game Over
                questionCount = 0;
                GameManager.instance.setUpEndScreen(liveCount);
            }

            else if(questionCount < assignedQuestionList.Count)
            {
            
                StartCoroutine(ShowCorrectPrompt(correctImage));
                Debug.Log("Showing Next Question");

            }
        }
        else
        {
            
            StartCoroutine(ShowWrongPrompt(wrongImage));
            ReduceLives();
            shakeCamera.StartShake();
            
            if(liveCount <= 0)
            {

                questionCount = 0;
                GameManager.instance.setUpEndScreen(liveCount);
            }
          
        }

    }

    public int ReduceLives()
    {
        //make Heart look disappear or  removed by changing color
        // answerSlots


        heartLives[liveCount - 1].color = Color.black;
        liveCount--;

        return liveCount;
    }

    void ResetLives()
    {
        foreach (Image i in heartLives)
        {
            i.color = Color.white;
        }
        liveCount = 3;
    }

    IEnumerator WaitForFinish()
    {
        yield return new WaitForSeconds(2);
        shelf3Template.SetActive(false);
        endOfStoryPrompt.SetActive(false);
        ShowNextQuestion();
    }

    IEnumerator ShowCorrectPrompt(GameObject prompt)
    {
        prompt.SetActive(true);
        prompt.GetComponent<AudioSource>().PlayOneShot(correctSFX);
        yield return new WaitForSeconds(2);
        ShowNextQuestion();
        prompt.SetActive(false);
    }

    IEnumerator ShowWrongPrompt(GameObject prompt)
    {
        prompt.SetActive(true);
        prompt.GetComponent<AudioSource>().PlayOneShot(wrongSFX);
        yield return new WaitForSeconds(2);
        prompt.SetActive(false);
    }

}
