using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class QuestionTemplateScript : MonoBehaviour
{
    public TextAsset tutorialTextAsset;
    private Queue<string> sentenceQueue;
    public List<string> sentences;
    
    public GameObject panel;
    public int tutorialCount;
    public bool isTutorialDone = false;
    // Start is called before the first frame update
    private void OnEnable()
    {
        sentenceQueue = new Queue<string>();
        panel.SetActive(true);
        isTutorialDone = false;
        InitializeText();
    }


    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            DisplayNextTutorialLine();
        }
    }

    void DisplayNextTutorialLine()
    {

        if(sentenceQueue.Count == 0)
        {
            isTutorialDone = true;
            panel.SetActive(false);
            this.enabled = false;
            return;

        }
        string currentLine = sentenceQueue.Dequeue();

        panel.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = currentLine;
    }

    void InitializeText()
    {
        sentences.Clear();
        foreach (string w in tutorialTextAsset.text.Split(new char[] { '\n' }))
        {
            sentences.Add(w);
        }
        
        foreach( string lines in sentences)
        {
            sentenceQueue.Enqueue(lines);
        }
        DisplayNextTutorialLine();

    }
}
