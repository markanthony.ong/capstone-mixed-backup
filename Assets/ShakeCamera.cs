using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeCamera : MonoBehaviour
{
    [SerializeField] private Transform transform;
    [SerializeField] private float shakeDuration = 0f;
    [SerializeField] private float shakeMagnitude = 0.7f;
    [SerializeField] private float dampingSpeed = 1.0f;
    [SerializeField] Vector3 initialPosition;

    [SerializeField] private bool startShake = false;
    private void Awake()
    {
        transform = this.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
      //  initialPosition = transform.position;
    }

    private void Update()
    {
        if(startShake)
        {
            startShake = false;
            StartCoroutine(Shaking());
        }
    }

    IEnumerator Shaking()
    {
        Vector3 startPos = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < shakeDuration )
        {
            elapsedTime += Time.deltaTime;
            transform.localPosition = initialPosition + Random.insideUnitSphere * shakeMagnitude;            
            yield return null;
        }

        transform.position = startPos;
    }

    public void StartShake()
    {
        startShake = true;
    }
}
