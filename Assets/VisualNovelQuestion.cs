using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VisualNovelQuestion : MonoBehaviour
{
    [TextArea(1,5)]
    public string questionString;
    public GameObject correctAnswerGameObject;
    public GameObject secondCorrectAnswerGameObject;
    public string correctWordString;
    bool isCorrect;


  

    public void OnButtonAnswerClicked(GameObject letterChoice)
    {
        if(letterChoice.GetComponent<TMP_Text>().text == correctAnswerGameObject.GetComponent<TMP_Text>().text ||
            letterChoice.GetComponent<TMP_Text>().text == secondCorrectAnswerGameObject.GetComponent<TMP_Text>().text)
        {
            isCorrect = true;
        }
        else
        {
            isCorrect = false;
            
        }

        DialogueManager._instance.CheckAnswer(isCorrect);
    }

    public void OnSubmitButtonClicked(TMP_InputField inputAnswer)
    {
        if(string.Equals(inputAnswer.text ,correctWordString , System.StringComparison.OrdinalIgnoreCase))
        {
            isCorrect = true;
            
        }
        else
        {
            isCorrect = false;
         
        }

        DialogueManager._instance.CheckAnswer(isCorrect);
    }

    //For Onboarding Specific Questions
    public void AlwaysCorrectButton()
    {
        DialogueManager._instance.CheckAnswer(true);
    }
    public bool GetIsCorrect()
    {
        return isCorrect;
    }

 
}
